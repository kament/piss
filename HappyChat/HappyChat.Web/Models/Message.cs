﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HappyChat.Web.Models
{
    public class Message
    {
        public long Id { get; set; }

        public string AuthorId { get; set; }

        public long ChatId { get; set; }

        public string Content { get; set; }

        public virtual Chat Chat { get; set; }

        public virtual ApplicationUser Author { get; set; }
    }
}