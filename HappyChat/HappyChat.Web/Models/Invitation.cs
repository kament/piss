﻿using System.ComponentModel.DataAnnotations.Schema;

namespace HappyChat.Web.Models
{
    public class Invitation
    {
        public long Id { get; set; }

        public InvitationStatus Status { get; set; }

        public string ReceiverId { get; set; }

        public string SenderId { get; set; }

        [ForeignKey("Chat")]
        public long ChatId { get; set; }

        public virtual ApplicationUser Receiver { get; set; }

        public virtual ApplicationUser Sender { get; set; }

        public virtual Chat Chat { get; set; }
    }

    public enum InvitationStatus
    {
        Pending = 0,
        Approved = 1,
        Declined = 2
    }
}