﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HappyChat.Web.Models
{
    public class Chat
    {
        private IList<ApplicationUser> users;
        private IList<Invitation> invitations;
        private IList<Message> messages;

        public Chat()
        {
            this.users = new List<ApplicationUser>();
            this.invitations = new List<Invitation>();
            this.messages = new List<Message>();
        }

        public long Id { get; set; }

        [MaxLength(100)]
        [MinLength(1)]
        public string Name { get; set; }

        public string CreatorId { get; set; }

        public virtual IList<Message> Messages
        {
            get { return this.messages; }
            set { this.messages = value; }
        }

        public virtual ApplicationUser Creator { get; set; }

        public virtual IList<Invitation> Invitations
        {
            get { return this.invitations; }
            set { this.invitations = value; }
        }

        public virtual IList<ApplicationUser> Users
        {
            get { return this.users; }
            set { this.users = value; }
        }
    }
}