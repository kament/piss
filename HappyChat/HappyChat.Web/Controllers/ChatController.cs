﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using HappyChat.Web.Models;
using Microsoft.AspNet.Identity;

namespace HappyChat.Web.Controllers
{
    [Authorize]
    public class ChatController : Controller
    {
        private ApplicationDbContext dbContext;

        public ChatController(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Create()
        {
            var model = new CreateChatViewModel();
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Create(CreateChatViewModel model)
        {
            var chat = new Chat();
            chat.Name = model.Name;
            
            var currentUserId = this.User.Identity.GetUserId<string>();
            chat.CreatorId = currentUserId;

            model.UserIds.Where(x => x != "false");
            foreach (var userId in model.UserIds)
            {
                if (dbContext.Users.Any(x => x.Id == userId))
                {
                    chat.Invitations.Add(new Invitation
                    {
                        Status = InvitationStatus.Pending,
                        ReceiverId = userId,
                        SenderId = currentUserId
                    });
                }
            }

            dbContext.Chats.Add(chat);

            await dbContext.SaveChangesAsync();

            return RedirectToAction("Create");
        }

        [HttpGet]
        public async Task<ActionResult> All()
        {
            List<Chat> chats = await AllChats();

            return View(chats);
        }

        private async Task<List<Chat>> AllChats()
        {
            var userId = this.User.Identity.GetUserId<string>();

            var chats = await dbContext
                .Chats
                .Where(x => x.CreatorId == userId || x.Invitations.Any(y => y.Status == InvitationStatus.Approved && y.ReceiverId == userId))
                .ToListAsync();
            return chats;
        }

        [HttpGet]
        public async Task<ActionResult> Search(string chatName)
        {
            var userId = this.User.Identity.GetUserId<string>();

            var chats = await dbContext
                .Chats
                .Where(x => x.Name != null && x.Name.Contains(chatName))
                .Where(x => x.CreatorId == userId || x.Invitations.Any(y => y.Status == InvitationStatus.Approved && y.ReceiverId == userId))
                .ToListAsync();

            return PartialView("_ChatList", chats);
        }

        [HttpGet]
        public async Task<ActionResult> ById(long chatId)
        {
            var userId = this.User.Identity.GetUserId<string>();
            ViewBag.userId = userId;

            var userName = this.User.Identity.GetUserName();
            ViewBag.userName = userName;

            var chat = await dbContext
                .Chats
                .Where(x => x.Id == chatId)
                .Where(x => x.CreatorId == userId || x.Invitations.Any(y => y.Status == InvitationStatus.Approved && y.ReceiverId == userId))
                .Include(x => x.Messages)
                .FirstOrDefaultAsync();

            if(chat == null)
            {
                return View("Error");
            }

            return View(chat);
        }
    }

    public class MessageViewModel
    {
        public long ChatId { get; set; }

        public string Content { get; set; }
    }

    public class CreateChatViewModel
    {
        [MaxLength(100)]
        [MinLength(1)]
        public string Name { get; set; }

        public string Email { get; set; }

        public IEnumerable<string> UserIds { get; set; }
    }
}