﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using HappyChat.Web.Models;
using Microsoft.AspNet.Identity;

namespace HappyChat.Web.Controllers
{
    [Authorize]
    public class InvitationController : Controller
    {
        private ApplicationDbContext dbContext;

        public InvitationController(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        [HttpGet]
        public async Task<ActionResult> Pending()
        {
            var userId = this.User.Identity.GetUserId<string>();
            var pendingInvitations = await dbContext
                .Invitations
                .Where(x => x.ReceiverId == userId && x.Status == InvitationStatus.Pending)
                .ToListAsync();

            return View(pendingInvitations);
        }

        [HttpPost]
        public async Task<ActionResult> Approve(long invitationId)
        {
            var userId = this.User.Identity.GetUserId<string>();
            var invitation = dbContext
                .Invitations
                .FirstOrDefault(x => x.Id == invitationId && x.ReceiverId == userId);

            if(invitation != null)
            {
                invitation.Status = InvitationStatus.Approved;
                await dbContext.SaveChangesAsync();
            }
            else
            {
                return View("Error");
            }

            return RedirectToAction("ById", "Chat", new { chatId = invitation.ChatId });
        }

        [HttpPost]
        public async Task<ActionResult> Decline(long invitationId)
        {
            var userId = this.User.Identity.GetUserId<string>();
            var invitation = dbContext
                .Invitations
                .FirstOrDefault(x => x.Id == invitationId && x.ReceiverId == userId);

            if (invitation != null)
            {
                invitation.Status = InvitationStatus.Declined;
                await dbContext.SaveChangesAsync();
            }
            else
            {
                return View("Error");
            }

            return RedirectToAction("Pending");
        }
    }
}