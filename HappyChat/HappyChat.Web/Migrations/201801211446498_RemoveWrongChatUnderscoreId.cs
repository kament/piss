namespace HappyChat.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveWrongChatUnderscoreId : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Invitations", "Chat_Id", "dbo.Chats");
            DropIndex("dbo.Invitations", new[] { "Chat_Id" });
            DropColumn("dbo.Invitations", "ChatId");
            RenameColumn(table: "dbo.Invitations", name: "Chat_Id", newName: "ChatId");
            AlterColumn("dbo.Invitations", "ChatId", c => c.Long(nullable: false));
            AlterColumn("dbo.Invitations", "ChatId", c => c.Long(nullable: false));
            CreateIndex("dbo.Invitations", "ChatId");
            AddForeignKey("dbo.Invitations", "ChatId", "dbo.Chats", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Invitations", "ChatId", "dbo.Chats");
            DropIndex("dbo.Invitations", new[] { "ChatId" });
            AlterColumn("dbo.Invitations", "ChatId", c => c.Long());
            AlterColumn("dbo.Invitations", "ChatId", c => c.Int(nullable: false));
            RenameColumn(table: "dbo.Invitations", name: "ChatId", newName: "Chat_Id");
            AddColumn("dbo.Invitations", "ChatId", c => c.Int(nullable: false));
            CreateIndex("dbo.Invitations", "Chat_Id");
            AddForeignKey("dbo.Invitations", "Chat_Id", "dbo.Chats", "Id");
        }
    }
}
