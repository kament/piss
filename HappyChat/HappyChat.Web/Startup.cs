﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HappyChat.Web.Startup))]
namespace HappyChat.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
