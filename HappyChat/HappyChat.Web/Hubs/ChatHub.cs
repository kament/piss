﻿using System;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;
using HappyChat.Web.Models;
using HappyChat.Web;
using System.Linq;

namespace SignalRChat
{
    public class ChatHub : Hub
    {
        public void Send(string userId, string userName, string message, string chatId)
        {
            var chatIdAsLong = Convert.ToInt64(chatId);

            var msg = new Message
            {
                AuthorId = userId,
                ChatId = chatIdAsLong,
                Content = message
            };

            using (var dbContext = new ApplicationDbContext())
            {
                if (CanSendMessage(chatIdAsLong, userId, dbContext))
                {
                    dbContext.Messages.Add(msg);
                    dbContext.SaveChanges();

                    Clients.Group(chatId).addNewMessageToPage(userName, message);
                }
            }
        }

        public Task JoinChat(string chatId)
        {
            return Groups.Add(Context.ConnectionId, chatId);
        }

        public Task LeaveChat(string chatId)
        {
            return Groups.Remove(Context.ConnectionId, chatId);
        }

        private bool CanSendMessage(long chatId, string userId, ApplicationDbContext dbContext)
        {
             return dbContext
                .Chats
                .Any(x => x.Id == chatId && x.CreatorId == userId || x.Invitations.Any(y => y.Status == InvitationStatus.Approved && y.ReceiverId == userId));
        }
    }
}
