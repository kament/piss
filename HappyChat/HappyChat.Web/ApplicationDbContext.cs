﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using HappyChat.Web.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace HappyChat.Web
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("HappyChatConnection", throwIfV1Schema: false)
        {
        }

        public DbSet<Chat> Chats { get; set; }

        public DbSet<Invitation> Invitations { get; set; }

        public DbSet<Message> Messages { get; set; }
        
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}